# Licence

Ce travail réalisé en 2024 par Jean-Baptiste Clad est placé sous licence [ CC BY-SA 4.0 ](https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1)